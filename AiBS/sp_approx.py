#!/usr/bin/python
import sys
import os.path
import StringIO
import itertools
from operator import itemgetter
from Bio import SeqIO
from random import shuffle

#Check user input
if len(sys.argv) < 4:
    sys.exit()
print 'Fasta file:\t', sys.argv[1]
print 'Score matrix:\t', sys.argv[2]
print 'Gap cost:\t', sys.argv[3]
if len(sys.argv) >= 5:
    print 'Max sequences:\t', sys.argv[4]
if len(sys.argv) >= 6:
    print 'Output fasta:\t', sys.argv[5]

def readScoreMatrix( filename ):
    if not os.path.isfile( filename):
        print "Score matrix file doesn't exist"
        sys.exit()
    f = open(filename, 'r')
    return [[int(c) for c in line.split() ] for line in f] 

def readFasta( filename ):
    if not os.path.isfile( filename):
        print "Sequence file doesn't exist"
        sys.exit()
    return SeqIO.read(filename, "fasta")

score_matrix_filename = sys.argv[2]
gap_cost = int(sys.argv[3])

handle = open( sys.argv[1], "rU")
sequences = list( SeqIO.parse(handle, "fasta"))
sequences = sequences[:int(sys.argv[4])]

def optimalAlignment(seq1fasta, seq2fasta, score_matrix, gap_cost):
    #Convert sequences to integers
    alpha = {'A' : 0, 'C' : 1, 'G' : 2, 'T' : 3, 'a' : 0, 'c' : 1, 'g' : 2, 't' : 3, '-' : 4, 'N': 0, 'n' : 0, 'R' : 0, 'Y' : 1, 'W' : 0, 'S' : 1, 'M' : 0, 'K' : 2, 'B' : 1, 'D' : 0, 'H' : 0, 'V' : 0}
    seq1 = [alpha[i] for i in seq1fasta.seq]
    seq2 = [alpha[i] for i in seq2fasta.seq]
    
    DP = [[0 for x in xrange(len(seq1)+1)] for x in xrange(len(seq2)+1)]
    BT = [[0 for x in xrange(len(seq1)+1)] for x in xrange(len(seq2)+1)]

    for i in xrange(len(seq1)+1):
        for j in xrange(len(seq2)+1):
            if i == 0 and j == 0:
                DP[j][i] = 0
            else:
                v0 = v1 = v2 = sys.maxint
                if i > 0 and j > 0:
                    v0 = DP[j-1][i-1] + score_matrix[seq1[i-1]][seq2[j-1]]
                if i > 0 :
                    v1 = DP[j][i-1] + gap_cost
                if j > 0 :
                    v2 = DP[j-1][i] + gap_cost
                DP[j][i] = min(v0, v1, v2)
                BT[j][i] = [v0, v1, v2].index( DP[j][i] )
    
    align1 = StringIO.StringIO()
    align2 = StringIO.StringIO()

    i = len(seq1) 
    j = len(seq2)
    while i > 0 or j > 0:
        if BT[j][i] == 0:
            align1.write( seq1fasta.seq[i-1] ) 
            align2.write( seq2fasta.seq[j-1] )
            i = i-1
            j = j-1
        if BT[j][i] == 1:
            align1.write( seq1fasta.seq[i-1] )
            align2.write( "-" )
            i = i-1
        if BT[j][i] == 2:
            align2.write( seq2fasta.seq[j-1] )
            align1.write( "-" )
            j = j-1
 
    return (DP[len(seq2)][len(seq1)], list(align1.getvalue()[::-1]), list(align2.getvalue()[::-1]))


# Run stuff
score_matrix = readScoreMatrix( score_matrix_filename )

# Calculate pairwise alignments and find central sequence
alignments = []
for i,seq1 in enumerate(sequences):
    alignments.append([])
    for j,seq2 in enumerate(sequences):
        if i == j:
            alignments[i].append( [0])
        if i < j :
            alignments[i].append( optimalAlignment(seq1,seq2, score_matrix, gap_cost))
        if j < i :
            alignments[i].append( [alignments[j][i][0], alignments[j][i][2], alignments[j][i][1] ])
            
        

#alignments = [[optimalAlignment(seq1,seq2, score_matrix, gap_cost) for seq2 in sequences] for seq1 in sequences]
dists = [sum( [x[0] for x in row]) for row in alignments]
center = min(enumerate(dists), key=itemgetter(1))[0]

print "Central sequence:{0}".format( sequences[center].id)

#Make alignment by adding pair wise alignment
MSA = [ ]
MSA_names = [ ]

MSA.append( [r for r in sequences[center].seq] )
MSA_names.append( sequences[center].id )

#print "MSA"
#print MSA[0]
for i,s in enumerate(alignments[center]):
    if i == center:
        continue

    #Add alignment
    MSA.append( ['-'] * len(MSA[0]) )
    MSA_names.append( sequences[i].id )

    #Glue with new alignments
    j = -1 #position in MSA 0-indxd
    chMSA = 0 #No. of characters seen in central sequence in MSA 
    chPA = 0 

    #chMSA characters in central sequence MSA and
    #chPA characeter in central sequence PA
    for s1,s2 in zip(s[1],s[2]):
        #Insert all nucl that match nucl in MSA[0]
        if(s1 == '-'):
            #insert gaps in all previous sequences and insert symbol
            while chMSA < chPA:
                j += 1
                if(MSA[0][j] != '-'):
                    chMSA += 1
            for align in MSA[:-1]:
                align.insert(j+1, '-')
            MSA[-1].insert(j+1,s2)
        else:
            chPA += 1
            #Increment j until chPA==MSA
            while chMSA < chPA:
                j += 1
                if(MSA[0][j] != '-' ):
                    chMSA += 1
            MSA[-1][j] = s2
                

#print "MSA:"
#for m in MSA:
#    print m
               

def costPair( s1, s2, score_matrix, gap):
    alpha = {'A' : 0, 'C' : 1, 'G' : 2, 'T' : 3, 'a' : 0, 'c' : 1, 'g' : 2, 't' : 3, '-' : 4, 'N': 0, 'n' : 0, 'R' : 0, 'Y' : 1, 'W' : 0, 'S' : 1, 'M' : 0, 'K' : 2, 'B' : 1, 'D' : 0, 'H' : 0, 'V' : 0}
    score_matrix[0].append( gap)
    score_matrix[1].append( gap)
    score_matrix[2].append( gap)
    score_matrix[3].append( gap)
    score_matrix.append( [gap, gap, gap, gap, 0] )

    seq1 = [alpha[i] for i in s1]
    seq2 = [alpha[i] for i in s2]
    return sum( [ score_matrix[i][j] for i,j in zip(seq1,seq2)])

#Find score of multiple alignment
score = 0
for i, s1 in enumerate(MSA):
    for j, s2 in enumerate(MSA):
        if i<j:
            #Calculate
            score += costPair( s1, s2, score_matrix, gap_cost)
            
print "Score:\t{0}".format(score) 


#Write output fasta

if len(sys.argv) >= 6:
    f = open( sys.argv[5], 'w')
    for i,s in enumerate(MSA):
        f.write( "> ")
        f.write( MSA_names[i])
        f.write( "\n")
        f.write( "".join(s) )
        f.write( "\n")
    


