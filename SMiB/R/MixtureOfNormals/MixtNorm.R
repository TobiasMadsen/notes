##--------------------------------------------------------
## Illustration of EM algorithm for mixture of two normals
##---------------------------------------------------------
## Simulate Data
alpha <- 0.2
nObs <- 100
x <- rbinom(n=nObs,size=1,alpha)
mu <- 4
y <- rnorm(n=nObs,mean=mu*x,sd=1)
## Illustrate data
s <- seq(-3,6,len=100)
hist(y,prob=TRUE,ylim=c(0,0.5))
lines(s,alpha*dnorm(s,mean=mu,sd=1),col="red",lwd=2)
lines(s,(1-alpha)*dnorm(s,mean=0,sd=1),col="red",lwd=2)
lines(s,alpha*dnorm(s,mean=mu,sd=1)+(1-alpha)*dnorm(s,mean=0,sd=1),
      col="red",lwd=4)
##------------------------------------------------
## EM algorithm
##------------------------------------------------
## Starting values
alphaEst <- 0.1
muEst <- 5
## Number of iterations
nIter <- 20
## Run EM algorithm
for (iter in 1:nIter){
  xPrb <- alphaEst*dnorm(y,mean=muEst,sd=1)/
    (alphaEst*dnorm(y,mean=muEst,sd=1)+
     (1-alphaEst)*dnorm(y,mean=0,sd=1))
  alphaEst <- sum(xPrb)/nObs
  muEst <- sum(xPrb*y)/sum(xPrb)
  logLk <- sum(log(alphaEst*dnorm(y,mean=muEst,sd=1)+
                   (1-alphaEst)*dnorm(y,mean=0,sd=1)))
  cat("Iteration:",iter,
      "alpha:",alphaEst,"mu:",muEst,
      "logLk",logLk,"\n")
}
## Illustrate final fit
lines(s,alphaEst*dnorm(s,mean=muEst,sd=1),col="blue",lwd=2)
lines(s,(1-alphaEst)*dnorm(s,mean=0,sd=1),col="blue",lwd=2)
lines(s,alphaEst*dnorm(s,mean=muEst,sd=1)+(1-alphaEst)*dnorm(s,mean=0,sd=1),
      col="blue",lwd=4)
## Add legend to plot
legend("topright",
       c("True mixture distribution","Estimated mixture distribution"),
       bty="n",col=c("red","blue"),lty=1,lwd=3)
