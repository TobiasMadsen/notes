#include <Rcpp.h>

using namespace Rcpp;

// Below is a simple example of exporting a C++ function to R. You can
// source this function into an R session using the Rcpp::sourceCpp 
// function (or via the Source button on the editor toolbar)

// For more on using Rcpp click the Help button on the editor toolbar

// [[Rcpp::export]]
int sample_rcpp(int n, NumericVector p){
  NumericVector u = runif(1);
  double cumsum = 0;
  for(int i = 0; i<n; ++i){
    cumsum += p(i);
    if(cumsum > u(0))
      return i+1;
  }
  return n;
}

// [[Rcpp::export]]
IntegerVector simulate_mc_cpp(NumericVector pi, NumericMatrix A, int L) {
  int n = pi.length();
  IntegerVector sim(L);

  sim(0) = sample_rcpp(n, pi);
  
  for(int i = 1; i < L; ++i){
    sim(i) = sample_rcpp(n,A(sim(i-1)-1,_));
  }

  return sim;
}
